
  
<?php

/**
 * @OA\Info(
 *     description="Dokumentasi API Tugas BE dari BUMA",
 *     version="1.0.0",
 *     title="Dokumentasi API Tugas BE dari BUMA",
 *     termsOfService="http://swagger.io/terms/",
 *     @OA\Contact(
 *         email="aghnatqiya@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
?>