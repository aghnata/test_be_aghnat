<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Stock;
use Illuminate\Support\Facades\File;

class StockController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/stocks",
     *     tags={"stocks"},
     *     summary="Menampilkan list barang",
     *     description="Dapat menampilkan list barang yang terdiri dari nomor, id barang, nama barang, jumlah stok",
     *     
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function index(){
        try {
            $stocks = DB::table('stocks')->select('id','name','qty')->get();;
    
            return response()->json([
                'is_success'   => true,
                'message' => 'load data success',
                'data' => $stocks
            ]);
            
        } catch (\Throwable $th) {

            return response()->json([
                'is_success'   => false,
                'message' => $th
            ]);
        }
    }


    /**
     * @OA\Get(
     *     path="/api/stocks/{id}",
     *     summary="Detail barang",
     *     description="Dapat menampilkan detail item barang yang terdiri dari id barang, nama barang, jumlah stok, status ketersediaan dan harga jual",
     *     operationId="getStockById",
     *     tags={"stocks"},
     *     @OA\Parameter(
     *         description="ID of pet to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function show($id){
        try {
            $stock = DB::table('stocks')
            ->where('id', $id)
            ->select('id','name','qty', 'price', 'available_status')->first();
    
            return response()->json([
                'is_success'   => true,
                'message' => 'load data success',
                'data' => $stock
            ]);
            
        } catch (\Throwable $th) {

            return response()->json([
                'is_success'   => false,
                'message' => $th
            ]);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/stocks",
     *     tags={"stocks"},
     *     operationId="storeStock",
     *     summary="Tambah barang",
     *     description="Dapat menambahkan item barang ke list barang",
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"name", "qty", "price", "available_status"},
     *       @OA\Property(property="name", type="string", example="chair"),
     *       @OA\Property(property="qty", type="integer", example=10),
     *       @OA\Property(property="price", type="decimal", example=3000000),
     *       @OA\Property(property="available_status", type="boolean", example=true),
     *    ),
     * ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function store(Request $request){
        try {
            $stock = new Stock;
            $stock->name = $request->json("name");
            $stock->qty = $request->json("qty");
            $stock->price = $request->json("price");
            $stock->available_status = $request->json("available_status");
            $stock->save();

            return response()->json([
                'is_success'   => true,
                'message' => 'added data successfully',
                'id' => $stock->id
            ]);
            
        } catch (\Throwable $th) {
            
            return response()->json([
                'is_success'   => false,
                'message' => $th
            ]);
        }
    }


    /**
     * @OA\Put(
     *     path="/api/stocks",
     *     tags={"stocks"},
     *     operationId="updateStock",
     *     summary="Update barang",
     *     description="Dapat mengedit detail item barang",
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"id"},
     *       @OA\Property(property="id", type="integer", example=1),
     *       @OA\Property(property="name", type="string", example="chair"),
     *       @OA\Property(property="qty", type="integer", example=10),
     *       @OA\Property(property="price", type="decimal", example=3000000),
     *       @OA\Property(property="available_status", type="boolean", example=true),
     *    ),
     * ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function update(Request $request){
        try {
            $stock = Stock::find($request->json("id"));
            $stock->name = $request->json("name");
            $stock->qty = $request->json("qty");
            $stock->price = $request->json("price");
            $stock->available_status = $request->json("available_status");
            $stock->save();

            return response()->json([
                'is_success'   => true,
                'message' => 'updated data successfully',
                'id' => $request->json("id")
            ]);
            
        } catch (\Throwable $th) {
            $check = Stock::find($request->json("id"));
            $err = !$check ? 'Data not found, please select other id' : $th;

            return response()->json([
                'is_success'   => false,
                'message' => $err
            ]);
        }
    }


    /**
     * @OA\Delete(
     *     path="/api/stocks/{id}",
     *     summary="Hapus barang",
     *     description="Dapat menghapus Item barang",
     *     operationId="deleteStock",
     *     tags={"stocks"},
     *     @OA\Parameter(
     *         description="id to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function destroy($id){

        $stock = Stock::find($id);

        if ($stock) {
            $stock->delete();
            return response()->json([
                'is_success'   => true,
                'message' => 'deleted data successfully',
            ]);
        } else {
            return response()->json([
                'is_success'   => false,
                'message' => 'data not found, please select another id',
            ]);
        }
    }

    
    public function uplodFile(Request $request, $id){
        // die('sasasas');
        // upload file
        $cv = $request->file;
        $extensionCV = $cv->getClientOriginalExtension();
        $destinationPathCV = public_path() . '/uploads/test';
        $cvName = $request->name . '.'.$extensionCV;
        $cv->move($destinationPathCV, $cvName,  File::get($cv));
        
        return response()->json([
            'is_success'   => true,
            'message' => 'uploaded file successfully',
        ]);
        // try {

        // } catch (\Throwable $th) {
        //     return response()->json([
        //         'is_success'   => false,
        //         'message' => $th
        //     ]);
        // }
    }


}
